package shop.velox.cart_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import reactor.core.publisher.Mono;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.api.dto.ItemDto;


@Tag(name = "Item", description = "the Item API")
@RequestMapping("/carts/{id:.*}/items")
public interface ItemController {

  @Operation(summary = "Gets an item by cartId and itemId", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = ItemDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found", content = @Content(schema = @Schema())),
  })
  @GetMapping(value = "/{itemId}")
  Mono<ResponseEntity<ItemDto>> getItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String cartId,
      @Parameter(description = "Id of the item. Cannot be empty.", required = true) @PathVariable("itemId") final String itemId,
      @Parameter(description = "Return item with updated content or not. If refresh is empty list, all updated content of item will be returned, otherwise the current state of the item")
      @RequestParam(name = "refresh", required = false, defaultValue = "") final List<String> refresh
    );

  @Operation(summary = "Adds an item to the cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Item Created", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "cart not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409", description = "an item with given articleId already exists", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "No price exists for given article", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "424", description = "not enough stock for given article", content = @Content(schema = @Schema()))
  })
  @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  Mono<ResponseEntity<CartDto>> addItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String cartId,
      @Parameter(description = "Item to create. Cannot be empty.", required = true) @RequestBody final ItemDto item,
      @Parameter(description = "Return cart with updated content or not. If refresh is empty list, all updated content of cart will be returned, otherwise the current state of the cart")
      @RequestParam(name = "refresh", required = false, defaultValue = "") final List<String> refresh
  );


  @Operation(summary = "Updates an item in the cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory data missing", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "424", description = "not enough stock for given article", content = @Content(schema = @Schema()))
  })
  @PatchMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<CartDto>> updateItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id,
      @Parameter(description = "Item to update. Cannot be empty.", required = true) @RequestBody final ItemDto item);


  @Operation(summary = "Removes an item from the cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found", content = @Content(schema = @Schema()))
  })
  @DeleteMapping(value = "/{itemId}")
  Mono<ResponseEntity<CartDto>> removeItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id,
      @Parameter(description = "Id of the item. Cannot be empty.", required = true) @PathVariable("itemId") final String itemId);
}
