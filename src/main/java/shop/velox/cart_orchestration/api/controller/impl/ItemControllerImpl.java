package shop.velox.cart_orchestration.api.controller.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.cart_orchestration.api.controller.ItemController;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.api.dto.ItemDto;
import shop.velox.cart_orchestration.converter.Converter;
import shop.velox.cart_orchestration.service.CartOrchestrationService;


@RestController
public class ItemControllerImpl implements ItemController {

  private CartOrchestrationService cartOrchestrationService;
  private final Converter<ItemDto, shop.velox.cart.api.dto.ItemDto> itemConverter;

  @Autowired
  public ItemControllerImpl(CartOrchestrationService cartOrchestrationService, Converter<ItemDto, shop.velox.cart.api.dto.ItemDto> itemConverter) {
    this.cartOrchestrationService = cartOrchestrationService;
    this.itemConverter = itemConverter;
  }

  @Override
  public Mono<ResponseEntity<ItemDto>> getItem(String cartId, String itemId, List<String> refresh) {
    return Mono.fromSupplier(() -> cartOrchestrationService.getItem(cartId, itemId, refresh));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> addItem(String cartId, ItemDto item, List<String> refresh) {
    shop.velox.cart.api.dto.ItemDto itemServiceDto = itemConverter.convertOrchestrationDtoToServiceDto(item);
    return Mono.fromSupplier(() -> cartOrchestrationService.addItem(cartId, itemServiceDto ,refresh));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> updateItem(String cartId, ItemDto item) {
    shop.velox.cart.api.dto.ItemDto itemServiceDto = itemConverter.convertOrchestrationDtoToServiceDto(item);
    return Mono.fromSupplier(() -> cartOrchestrationService.updateItem(cartId, itemServiceDto));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> removeItem(String id, String itemId) {
    return Mono.fromSupplier(() ->
        cartOrchestrationService.removeItem(id, itemId));
  }

}
