package shop.velox.cart_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Mono;
import shop.velox.cart_orchestration.api.dto.CartDto;


@Tag(name = "Cart", description = "the Cart API")
@RequestMapping("/carts")
public interface CartController {

  @Operation(summary = "create new Cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Cart created", content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<CartDto>> createCart(
      @Parameter(description = "The cart to create.") @Valid @RequestBody final CartDto cart
  );

  @Operation(summary = "Find Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "403", description = "cart does not exist or belongs to other user", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "404", description = "cart not found", content = @Content(schema = @Schema()))
  })
  @GetMapping(value = "/{id:.*}", produces = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<CartDto>> getCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id,
      @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToCustomerIdMapper.apply(#this)") String customerId,
      @Parameter(description = "Return cart with updated content or not. If refresh is empty list, all updated content of cart will be returned, otherwise the current state of the cart")
      @RequestParam(name = "refresh", required = false, defaultValue = "") final List<String> refresh
  );

  @Operation(summary = "Delete Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "403", description = "cart does not exist or belongs to other user"),
      @ApiResponse(responseCode = "404", description = "cart not found (if invoked by admin)")
  })
  @DeleteMapping(value = "/{id:.*}")
  Mono<ResponseEntity<Void>> removeCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String id);

  @Operation(summary = "gets all carts. Paginated.", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<String> getCarts(Pageable pageable);

  @Operation(summary = "Update cart", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Cart updated", content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "403", description = "Not allowed to update the cart"),
      @ApiResponse(responseCode = "404", description = "Cart for the given cart ID not found"),
      @ApiResponse(responseCode = "422", description = "Cart can't be updated")
  })
  @PatchMapping(value = "/{id:.*}", produces = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<CartDto>> updateCart(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true) @PathVariable("id") final String cartId,
      @Parameter(description = "The cart to update. Cannot be empty.", required = true) @RequestBody final CartDto cart,
      @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToCustomerIdMapper.apply(#this)") final String customerId
  );
}
