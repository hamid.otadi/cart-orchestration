package shop.velox.cart_orchestration.api.controller.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.cart_orchestration.api.controller.CartController;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.converter.Converter;
import shop.velox.cart_orchestration.service.CartOrchestrationService;


@RestController
public class CartControllerImpl implements CartController {

  private final CartOrchestrationService cartOrchestrationService;
  private final Converter<CartDto, shop.velox.cart.api.dto.CartDto> cartConverter;

  @Autowired
  public CartControllerImpl(CartOrchestrationService cartOrchestrationService, Converter<CartDto, shop.velox.cart.api.dto.CartDto> cartConverter) {
    this.cartOrchestrationService = cartOrchestrationService;
    this.cartConverter = cartConverter;
  }

  @Override
  public Mono<ResponseEntity<CartDto>> createCart(final CartDto cart) {
    shop.velox.cart.api.dto.CartDto cartServiceDto = cartConverter.convertOrchestrationDtoToServiceDto(cart);

    return Mono.fromSupplier(() -> cartOrchestrationService.createCart(cartServiceDto));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> getCart(final String id, final String customerId,
      final List<String> refresh) {
    return Mono.fromSupplier(() -> cartOrchestrationService.getCart(id, customerId, refresh));
  }

  @Override
  public Mono<ResponseEntity<Void>> removeCart(final String cartId) {
    return Mono.fromSupplier(() -> cartOrchestrationService.removeCart(cartId));
  }

  @Override
  public ResponseEntity<String> getCarts(final Pageable pageable) {
    return cartOrchestrationService.getCarts(pageable);
  }

  @Override
  public Mono<ResponseEntity<CartDto>> updateCart(String cartId, CartDto cart, String customerId) {
    shop.velox.cart.api.dto.CartDto cartServiceDto = cartConverter.convertOrchestrationDtoToServiceDto(cart);
    return Mono.fromSupplier(() -> cartOrchestrationService.updateCart(cartId, cartServiceDto, customerId));
  }

}
