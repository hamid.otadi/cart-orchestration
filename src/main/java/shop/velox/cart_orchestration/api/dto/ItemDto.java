package shop.velox.cart_orchestration.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.catalog.model.ProductEntity;

@JsonInclude(Include.NON_NULL)
public class ItemDto {

  @Schema(description = "Unique identifier of the Item.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private String id;

  @Schema(description = "Unique identifier of the Item.", example = "article1", required = true)
  private String articleId;

  @Schema(description = "Name of the Item.", example = "notebook")
  @JsonInclude(Include.ALWAYS)
  private String name;

  @Schema(description = "Quantity of articles of the Item.", example = "10", required = true)
  private BigDecimal quantity;

  @Schema(description = "Total price of the Item.", example = "100.10")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal price;

  @Schema(description = "Unit Price of the Item.", example = "10.01")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal unitPrice;

  @Schema(description = "Availability of the Item.", example = "100")
  @JsonInclude(Include.ALWAYS)
  private AvailabilityDto availability;

  @Schema(description = "Catalog information of an item.")
  @JsonInclude(Include.ALWAYS)
  private ProductEntity product;

  @Schema(description = "Indicates whether an Item can be ordered", example = "true")
  @JsonInclude(Include.ALWAYS)
  private boolean orderable;

  @Schema(description = "The messages on the item", example = "{level=ERROR, message=Price for this item does not exist}")
  private List<MessageDto> messages;

  public ItemDto(String id, String articleId, String name, BigDecimal quantity,
      BigDecimal price, BigDecimal unitPrice,
      AvailabilityDto availability, ProductEntity product, boolean orderable,
      List<MessageDto> messages) {
    this.id = id;
    this.articleId = articleId;
    this.name = name;
    this.quantity = quantity;
    this.price = price;
    this.unitPrice = unitPrice;
    this.availability = availability;
    this.product = product;
    this.orderable = orderable;
    this.messages = messages;
  }

  public ItemDto(){
    this.messages = new ArrayList<>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public AvailabilityDto getAvailability() {
    return availability;
  }

  public void setAvailability(AvailabilityDto availability) {
    this.availability = availability;
  }

  public ProductEntity getProduct() {
    return product;
  }

  public void setProduct(ProductEntity product) {
    this.product = product;
  }

  public boolean isOrderable() {
    return orderable;
  }

  public void setOrderable(boolean orderable) {
    this.orderable = orderable;
  }

  public List<MessageDto> getMessages() {
    return messages;
  }

  public void setMessages(List<MessageDto> messages) {
    this.messages = messages;
  }

  public void addMessage(MessageDto message) {
    this.messages.add(message);
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

}
