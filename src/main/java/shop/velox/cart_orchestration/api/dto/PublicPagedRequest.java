package shop.velox.cart_orchestration.api.dto;

import org.springframework.data.domain.PageRequest;

public class PublicPagedRequest extends PageRequest {

  public PublicPagedRequest(int page, int size) {
    super(page, size, null);
  }
}
