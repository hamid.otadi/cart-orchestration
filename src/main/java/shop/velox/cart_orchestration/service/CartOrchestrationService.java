package shop.velox.cart_orchestration.service;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.api.dto.ItemDto;

public interface CartOrchestrationService {

  /**
   * Creates a cart and returns it
   *
   * @param cartServiceDto the cart to create
   * @return the just created cart
   */
  ResponseEntity<CartDto> createCart(final shop.velox.cart.api.dto.CartDto cartServiceDto);

  /**
   * Gets a cart by its id
   *
   * @param cartId     the id of the cart
   * @param customerId the id of customer that wants to fetch the cart
   * @param refresh    the list of attributes to be returned for items in the cart
   * @return the cart wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<CartDto> getCart(final String cartId, final String customerId,
      final List<String> refresh);

  /**
   * Removes a cart with given id
   *
   * @param cartId the id of the cart
   * @return the void wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<Void> removeCart(final String cartId);

  /**
   * Gets all carts
   *
   * @param pageable information about the pagination
   * @return the string wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<String> getCarts(final Pageable pageable);

  /**
   * Updates a cart.
   *
   * @param cartId
   * @param cartServiceDto       contains the cart information
   * @param customerId
   * @return the cart wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<CartDto> updateCart(final String cartId, final shop.velox.cart.api.dto.CartDto cartServiceDto, final String customerId);

  /**
   * Gets an item by cart id and item id
   *
   * @param cartId  the id of the cart
   * @param itemId  the id of the item
   * @param refresh the list of attributes to be returned for item
   * @return the item wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<ItemDto> getItem(final String cartId, String itemId, final List<String> refresh);

  /**
   * Creates an item in the cart with id cartId
   *
   * @param cartId  the id of the cart
   * @param itemServiceDto    contains the information related to the item
   * @param refresh the list of attributes to be returned for items in the cart
   * @return the updated cart wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<CartDto> addItem(final String cartId, final shop.velox.cart.api.dto.ItemDto itemServiceDto, final List<String> refresh);

  /**
   * Updates an item in the cart with id cartId
   *
   * @param cartId the id of the cart
   * @param itemServiceDto   contains the information related to the item
   * @return the updated cart wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<CartDto> updateItem(final String cartId, final shop.velox.cart.api.dto.ItemDto itemServiceDto);

  /**
   * Removes an item with given id from cart with given id
   *
   * @param cartId the id of the cart
   * @param itemId the id of the item
   * @return the updated cart wrapped in a {@link ResponseEntity}
   */
  ResponseEntity<CartDto> removeItem(final String cartId, final String itemId);

}
