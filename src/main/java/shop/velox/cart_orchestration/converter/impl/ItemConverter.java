package shop.velox.cart_orchestration.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.cart_orchestration.converter.Converter;

@Component
public class ItemConverter implements Converter<shop.velox.cart_orchestration.api.dto.ItemDto, shop.velox.cart.api.dto.ItemDto> {

  private static final Logger LOG = LoggerFactory.getLogger(ItemConverter.class);
  private final MapperFacade mapperFacade;

  public ItemConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(shop.velox.cart_orchestration.api.dto.ItemDto.class, shop.velox.cart.api.dto.ItemDto.class)
        .byDefault()
        .fieldAToB("price", "totalPrice")
        .exclude("availability")
        .exclude("orderable")
        .exclude("product")
        .exclude("messages")
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public shop.velox.cart.api.dto.ItemDto convertOrchestrationDtoToServiceDto(shop.velox.cart_orchestration.api.dto.ItemDto itemOrchestrationDto) {
    shop.velox.cart.api.dto.ItemDto itemServiceDto = mapperFacade.map(itemOrchestrationDto, shop.velox.cart.api.dto.ItemDto.class);
    LOG.debug("Converted {} to {}", itemOrchestrationDto, itemServiceDto);
    return itemServiceDto;
  }

  @Override
  public shop.velox.cart_orchestration.api.dto.ItemDto convertServiceDtoToOrchestrationDto(shop.velox.cart.api.dto.ItemDto itemServiceDto) {
    shop.velox.cart_orchestration.api.dto.ItemDto itemOrchestrationDto = mapperFacade.map(itemServiceDto, shop.velox.cart_orchestration.api.dto.ItemDto.class);
    LOG.debug("Converted {} to {}", itemServiceDto, itemOrchestrationDto);
    return itemOrchestrationDto;
  }

}
