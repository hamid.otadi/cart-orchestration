package shop.velox.cart_orchestration.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.cart_orchestration.converter.Converter;

@Component
public class CartConverter implements Converter<shop.velox.cart_orchestration.api.dto.CartDto, shop.velox.cart.api.dto.CartDto> {

  private static final Logger LOG = LoggerFactory.getLogger(CartConverter.class);
  private final MapperFacade mapperFacade;

  public CartConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(shop.velox.cart_orchestration.api.dto.CartDto.class, shop.velox.cart.api.dto.CartDto.class)
        .byDefault()
        .exclude("total")
        .exclude("orderable")
        .exclude("messages")
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public shop.velox.cart.api.dto.CartDto convertOrchestrationDtoToServiceDto(shop.velox.cart_orchestration.api.dto.CartDto cartOrchestrationDto) {
    shop.velox.cart.api.dto.CartDto cartServiceDto = mapperFacade.map(cartOrchestrationDto, shop.velox.cart.api.dto.CartDto.class);
    LOG.debug("Converted {} to {}", cartOrchestrationDto, cartServiceDto);
    return cartServiceDto;
  }

  @Override
  public shop.velox.cart_orchestration.api.dto.CartDto convertServiceDtoToOrchestrationDto(shop.velox.cart.api.dto.CartDto cartServiceDto) {
    shop.velox.cart_orchestration.api.dto.CartDto cartOrchestrationDto = mapperFacade.map(cartServiceDto, shop.velox.cart_orchestration.api.dto.CartDto.class);
    LOG.debug("Converted {} to {}", cartServiceDto, cartOrchestrationDto);
    return cartOrchestrationDto;
  }

}
