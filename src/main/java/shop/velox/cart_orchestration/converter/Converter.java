package shop.velox.cart_orchestration.converter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.ListUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface Converter<ORCHESTRATION_DTO, SERVICE_DTO> {

  SERVICE_DTO convertOrchestrationDtoToServiceDto(ORCHESTRATION_DTO orchestrationDto);

  ORCHESTRATION_DTO convertServiceDtoToOrchestrationDto(SERVICE_DTO serviceDto);

  default List<SERVICE_DTO> convertOrchestrationDtoToServiceDto(List<ORCHESTRATION_DTO> orchestrationDtoList) {
    return ListUtils.emptyIfNull(orchestrationDtoList)
        .stream()
        .filter(Objects::nonNull)
        .map(this::convertOrchestrationDtoToServiceDto)
        .collect(Collectors.toList());
  }

  default List<ORCHESTRATION_DTO> convertServiceDtoToOrchestrationDto(List<SERVICE_DTO> serviceDtoList) {
    return ListUtils.emptyIfNull(serviceDtoList)
        .stream()
        .filter(Objects::nonNull)
        .map(this::convertServiceDtoToOrchestrationDto)
        .collect(Collectors.toList());
  }

  default Page<SERVICE_DTO> convert(Pageable pageable, Page<ORCHESTRATION_DTO> orchestrationDtoPage) {
    return new PageImpl<>(this.convertOrchestrationDtoToServiceDto(orchestrationDtoPage.getContent()),
        pageable, orchestrationDtoPage.getTotalElements());
  }
}
